﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Dreamteck.Splines;

public class test : MonoBehaviour {

	public SplineComputer farm;
	public SplineComputer newSpline;
	public Node node1;
	public Node node2;
	public SplineComputer extensionSplinePrefab;
	private GameObject[] farmNodes;
	public GameObject nodePrefab;
	private int farmPointCount;

	private SplineComputer farm2;

	void Start ()
	{
		SplinePoint[] farmPoints = new SplinePoint[farm.pointCount];
		farmPoints = farm.GetPoints ();
		farmPointCount = farm.pointCount;// - 1;//closed spline as one extra point (ends where it starts)

		farmNodes = new GameObject [farmPointCount];

		for (int itr = 0; itr < farmPointCount ; itr++)
		{
			farmNodes [itr] = Instantiate (nodePrefab);
			farmNodes[itr].transform.parent = farm.transform;
			Node node = farmNodes [itr].GetComponent<Node> ();
			node.transform.localRotation = Quaternion.identity;
			node.transform.position = farmPoints [itr].position;
			node.AddConnection (farm, itr);
			farmNodes[itr].AddComponent<BoxCollider> ().size = new Vector3 (0.0001f, 0.0001f, 0.0001f);
		}
			
		node1.gameObject.AddComponent<BoxCollider> ().size = new Vector3 (0.0001f, 0.0001f, 0.0001f);
		node2.gameObject.AddComponent<BoxCollider> ().size = new Vector3 (0.0001f, 0.0001f, 0.0001f);

		RaycastHit[] hits = new RaycastHit[farmPointCount + 2];
		double[] percents = new double[farmPointCount + 2];
		farm.RaycastAll(out hits, out percents, 1<<0);

		for (int itr = 0; itr < farmPointCount ; itr++)
		{
			farmNodes [itr].GetComponent<BoxCollider> ().enabled = false;
		}
		node1.gameObject.GetComponent<BoxCollider> ().enabled = false;
		node2.gameObject.GetComponent<BoxCollider> ().enabled = false;

		SplinePoint[] newFarmPoints = new SplinePoint[farmPointCount + 2];

		int ctr = farmPointCount + 1;
		for (int itr = 0; itr < farmPointCount + 2; itr++) 
		{
			newFarmPoints[itr].position = hits [itr].point;
			ctr = (ctr + 1) % (farmPointCount + 2);
		}
			
		farm2 = (SplineComputer) Instantiate (extensionSplinePrefab);
		farm2.SetPoints(newFarmPoints);
		farm2.precision = 0.9;
		farm2.gameObject.AddComponent<SurfaceGenerator> ();
		print ("New spline has been created!");
	}
	
	void Update () {
		
	}
}