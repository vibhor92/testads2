﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FlowManager : MonoBehaviour {

	private bool _musicOn = true;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void switchScene(string sceneName)
	{
		SceneManager.LoadSceneAsync (sceneName);
	}

	public void toggleMusic()
	{
		if (_musicOn) {
			_musicOn = false;

		}
	}
}
