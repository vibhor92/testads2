﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Dreamteck.Splines;

public class GameController : MonoBehaviour {

	public SplineComputer farm;
	public LineRenderer linePrefab;
	private LineRenderer newLine;
	private List<Vector3> linePoints;

	private float panSpeed = 0.025f;
	//Note to self - Make panSpeed dependent on farm size and allow it to be set by user via the menu
	public SplineComputer extensionSplinePrefab;
	private SplineComputer newSpline;

	private float orthoZoomSpeed = 0.025f;        // The rate of change of the orthographic size in orthographic mode.
	//Note to self - Make zoom speed dependent on farm size and allow it to be set by user via the menu
	private float maxZoom;
	private float minZoom = 2;

	private float rotationSpeed = 1.0f;
	private float oldAngle = -999;
	private bool continued2TouchWith2Fingers = false;

	//indicates if click/touch began from within the farm's area
	private bool eligibleForRecordingPoints = false;
	private bool recordPoints = false;
	private bool recordedPointsOnce = false;

	private List<SplinePoint> points;

	//Store cumulative touch.delta and check if greater than threshold
	private float deltaDistance;

	bool isOverObject(string objectName, Vector2 position)
	{
		Ray ray = Camera.main.ScreenPointToRay (new Vector3 (position.x, position.y, 0));
		RaycastHit hit;
		if (Physics.Raycast (ray, out hit))
		{
			if (hit.transform.name == objectName)
				return true;
			else
				return false;
		} else
			return false;
	}
		
	void Start () 
	{
		maxZoom = Camera.main.orthographicSize;
		points = new List<SplinePoint>();
		linePoints = new List<Vector3> ();
	}

	void Update () 
	{
		Touch[] touches = new Touch[2];
		int touchCount = Input.touchCount;
		if (touchCount == 1)
			touches [0] = Input.GetTouch (0);
		else if (touchCount == 2)
			touches = Input.touches;
		switch (touchCount) 
		{
			case 1://Either draw extention or pan
			{
				continued2TouchWith2Fingers = false;

				//Pan
				if(touches[0].phase == TouchPhase.Began)
				{
					eligibleForRecordingPoints = isOverObject ("Farm", touches[0].position);
					if (eligibleForRecordingPoints)
						recordPoints = false;
				}
				if (!eligibleForRecordingPoints && touches[0].phase == TouchPhase.Moved) {
					Vector2 touchDeltaPosition = touches[0].deltaPosition;
					Camera.main.transform.Translate(-touchDeltaPosition.x * panSpeed, -touchDeltaPosition.y * panSpeed, 0);
				}

				if (touches [0].phase == TouchPhase.Ended)
				{
					linePoints.Clear ();
					points.Clear ();
					if (newLine) 
						Destroy (newLine.gameObject);

					//prepare for next session by resetting information pertaining to previous session
					recordedPointsOnce = false;
				}

				//if finger is placed inside farm area and begins to move
				if (eligibleForRecordingPoints && touches [0].phase == TouchPhase.Moved)
				{
					//cumulate finger movement delta positions in deltaDistance
					deltaDistance += touches [0].deltaPosition.magnitude;

					//start recording points when finger reaches the boundary of the farm going outwards
					if (!isOverObject ("Farm", touches [0].position))
					{
						if (!recordPoints && !recordedPointsOnce)
						{
							//Delete previous spline if it exists
							if (newSpline) 
							{
								Destroy (newSpline.gameObject);
								points.Clear ();
							}
							if (newLine) 
							{
								Destroy (newLine.gameObject);
								linePoints.Clear ();
							}

							//Instantiate new line renderer for this extension session
							newLine = (LineRenderer) Instantiate (linePrefab);

							//record first point of current session
							Ray ray = Camera.main.ScreenPointToRay (new Vector3 (touches[0].position.x, touches[0].position.y, 0));
							Vector3 firstPoint = new Vector3 (ray.origin.x, ray.origin.y, 0);
							points.Add (new SplinePoint (firstPoint));



							if(newLine)
							{
								linePoints.Add (firstPoint);
								newLine.positionCount = linePoints.Count;
								newLine.SetPosition(linePoints.Count - 1, (Vector3)linePoints [linePoints.Count - 1]);
							}


						}
						//set flag to record other points of this session if this is first session
						if(!recordedPointsOnce)
							recordPoints = true;	
					}
					//when finger reaches back into the farm area, end recording points coz the session of extension marking is getting over
					else 
					{
						//lift finger after entering the farm from outside and touch screen again for next session
						if (recordPoints && !recordedPointsOnce)
						{
							//record last point of current session
							Ray ray = Camera.main.ScreenPointToRay (new Vector3 (touches[0].position.x, touches[0].position.y, 0));
							Vector3 lastPoint = new Vector3 (ray.origin.x, ray.origin.y, 0);
							points.Add (new SplinePoint (lastPoint));
							SplinePoint[] pointsArray = new SplinePoint[points.Count];
							points.CopyTo (pointsArray);

							//Instantiate and create new spline for this extension
							newSpline = (SplineComputer) Instantiate (extensionSplinePrefab);
							newSpline.precision = 0.5;
							newSpline.SetPoints(pointsArray);

							GameObject firstPointNode = new GameObject ("FirstPointNode");
							firstPointNode.transform.parent = newSpline.transform;
							Node node1 = firstPointNode.AddComponent<Node>();
							node1.transform.localRotation = Quaternion.identity;
							node1.transform.position = points [0].position;
							node1.AddConnection(newSpline, 0);

							GameObject obj1 = new GameObject ("Point Finder");
							SplineUser posFinder = obj1.AddComponent<SplineUser> ();
							posFinder.computer = farm;
							SplineResult pos = posFinder.Project (node1.transform.position);
							firstPointNode.transform.position = pos.position;

							GameObject lastPointNode = new GameObject ("LastPointNode");
							lastPointNode.transform.parent = newSpline.transform;
							Node node2 = lastPointNode.AddComponent<Node> ();
							node2.transform.localRotation = Quaternion.identity;
							node2.transform.position = points [points.Count-1].position;
							node2.AddConnection(newSpline, points.Count-1);

							pos = posFinder.Project (node2.transform.position);
							lastPointNode.transform.position = pos.position;

							Destroy (obj1);
							Destroy (newLine.gameObject);
							newLine = null;
							linePoints.Clear ();

							//stop recording points
							recordedPointsOnce = true;
							points.Clear ();
						}
						recordPoints = false;
					}

					//if deltaDistance reaches threshold, record a point
					if (deltaDistance > 30) 
					{
						deltaDistance = 0;
						if (recordPoints && !recordedPointsOnce) 
						{
							Ray ray = Camera.main.ScreenPointToRay (new Vector3 (touches[0].position.x, touches[0].position.y, 0));
							Vector3 pointOnCurve = new Vector3 (ray.origin.x, ray.origin.y, 0);
							points.Add (new SplinePoint (pointOnCurve));

							if(newLine)
							{
								linePoints.Add (pointOnCurve);
								newLine.positionCount = linePoints.Count;
								newLine.SetPosition(linePoints.Count - 1, (Vector3)linePoints [linePoints.Count - 1]);
							}
						}
					}
				}

				break;
			}
			case 2://Zoom in/out or rotate
			{

				Vector2 touchZeroPrevPos = touches[0].position - touches[0].deltaPosition;
				Vector2 touchOnePrevPos = touches[1].position - touches[1].deltaPosition;

				// Find the magnitude of the vector (the distance) between the touches in each frame.
				float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
				float touchDeltaMag = (touches[0].position - touches[1].position).magnitude;

				// Find the difference in the distances between each frame.
				float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

				Vector2 v2 = touches [0].position - touches [1].position;
				float newAngle = Mathf.Atan2 (v2.y, v2.x) * Mathf.Rad2Deg;
				if(!continued2TouchWith2Fingers)
					oldAngle = newAngle;
				float deltaAngle = Mathf.DeltaAngle (newAngle, oldAngle);
				oldAngle = newAngle;

				// If the camera is orthographic...
				if (Camera.main.orthographic)
				{
					// Change the orthographic size based on the change in distance between the touches.
					Camera.main.orthographicSize += deltaMagnitudeDiff * orthoZoomSpeed;

					// Make sure the orthographic size never drops below zero.
					Camera.main.orthographicSize = Mathf.Max(Camera.main.orthographicSize, minZoom);

					// Make sure the orthographic size never goes above original size.
					Camera.main.orthographicSize = Mathf.Min(Camera.main.orthographicSize, maxZoom);

					Camera.main.transform.Rotate (0, 0, deltaAngle * rotationSpeed);
				}

				continued2TouchWith2Fingers = true;
				break;
			}
			default:
			{
				continued2TouchWith2Fingers = false;
				break;
			}
		}

	}//end of Update

}